#!/usr/bin/env python3
"""Returning the location of the ISS in latitude/longitude"""
import requests
import time
import reverse_geocoder as rg

URL= "http://api.open-notify.org/iss-now.json"
def main():
    resp= requests.get(URL).json()

    lat=resp["iss_position"] ["latitude"]
    lon=resp["iss_position"] ["longitude"]
    ts= resp["timestamp"]

    hr_ts= time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
    locator_resp= rg.search((lat, lon))
    city= locator_resp[0]["name"]
    country= locator_resp[0]["cc"]


    print(f"""
CURRENT LOCATION OF THE ISS:
Timestamp:{hr_ts} 
Lon: {lon}
Lat: {lat}
City/Country: {city},  {country}
""")

if __name__ == "__main__":
    main()
